package com.kontakto.view.json

/**
 * Created by dodywicaksono on 2/15/14.
 */
class ContactView {
    Integer accountNumber
    String firstName
    String lastName
    String email
    String address
    String city
    String postCode
    String province
    String country
    String phone
    String fax
}
