package com.kontakto

import grails.converters.JSON
import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.annotation.Secured
import grails.web.JSONBuilder

import javax.servlet.http.HttpServletRequest
import javax.ws.rs.FormParam
import javax.ws.rs.GET
import javax.ws.rs.POST
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.Context
import javax.ws.rs.core.MediaType

@Path('/api/contact')
class ContactResource {

    @Secured
    @POST
    @Path('/getAll')
    @Produces(MediaType.APPLICATION_JSON)
    String getAll(@Context HttpServletRequest context,
                  @FormParam('accountNumber') String accountNumber) {

//        SpringSecurityService springSecurityService

        println("accountNumber: "+accountNumber)
//        println("accountNumber: "+accountNumber)

//        springSecurityService.principal

        if (accountNumber != null) {

            def contactList = Contact.findAllByAccountNumber(accountNumber)

            def builder = new JSONBuilder()

            String result = builder.build() {
                setProperty("Status", "1")
                setProperty("contactList", cl)
            }

            return result

        }
        else {
            BusinessException result = new BusinessException("CR-0001", "Invalid account number")
            return result as JSON
        }

        


    }
}
