package com.kontakto

class Contact {

    Integer accountNumber
    String firstName
    String lastName
    String email
    String address
    String city
    String postCode
    String province
    String country
    String phone
    String fax

    Contact() { }

    static constraints = {
        firstName (blank:false, nullable:false)
        firstName (blank:false, nullable:false)
        lastName (blank:true, nullable:true)
        email (blank:true, nullable:true)
        address (blank:true, nullable:true)
        city (blank:true, nullable:true)
        postCode (blank:true, nullable:true)
        province (blank:true, nullable:true)
        country (blank:true, nullable:true)
        phone (blank:true, nullable:true)
        fax (blank:true, nullable:true)
    }

    String toString(){
        return firstName+" "+lastName;
    }

}
