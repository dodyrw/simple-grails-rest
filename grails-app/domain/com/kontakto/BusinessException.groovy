package com.kontakto

class BusinessException {

    String errorCode
    String fullMessage

    BusinessException() {
    }

    BusinessException(errorCode, fullMessage) {
        this.errorCode = errorCode
        this.fullMessage = fullMessage
    }

    static constraints = {
    }
}
