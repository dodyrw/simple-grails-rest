package com.kontakto

class Role {

	String authority

	static mapping = {
        table 'auth_role'
		cache true
	}

	static constraints = {
		authority blank: false, unique: true
	}
}
