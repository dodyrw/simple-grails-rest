import com.kontakto.Contact
import com.kontakto.Role
import com.kontakto.User
import com.kontakto.UserRole

class BootStrap {

    def init = { servletContext ->

        /*
        spring security setup
         */

        def adminRole = new Role(authority: 'ROLE_ADMIN').save(flush: true)
        def userRole = new Role(authority: 'ROLE_USER').save(flush: true)

        def testUser = new User(accountNumber: 1001, username: 'admin', password: 'admin')
        testUser.save(flush: true)

        UserRole.create testUser, adminRole, true

        assert User.count() == 1
        assert Role.count() == 2
        assert UserRole.count() == 1


        /*
        sample data
         */

        println('create sample data')
        new Contact(accountNumber: 1001, firstName: "Dody", lastName: "Wicaksono", email: "dodyrw@gmail.com").save()
        new Contact(accountNumber: 1001, firstName: "Amik", lastName: "Aja").save()
        new Contact(accountNumber: 1001, firstName: "Afandy").save()
        new Contact(accountNumber: 1001, firstName: "Afandy").save()

    }
    def destroy = {
    }
}
